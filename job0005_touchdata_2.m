datapath = '/home/shige-o/data/2013GayaFRET/'

fnbase = 'YFP_CFP5';
%fnbase = 'YFP_CFP7';
tiffile = [datapath, fnbase, '.tif']
outmatfile = [fnbase '.mat'];

Nx = 270; Ny = 252; Nz = 1; Nf = 350;
info = imfinfo( tiffile );

x = zeros( Ny, Nx, Nf, 'uint8' );
for f=1:Nf
    [x1, map] = imread( tiffile, f,  'Info',info );
    x( :, :, f ) = x1;
    image( x1 )
    title(sprintf( 'f=%d',f ) )
    drawnow
end

xpyramid = im_pyramid( x, 5 );
save( outmatfile, 'x', 'xpyramid' )
%%
load( outmatfile );
for f=1:Nf
    for l=1:5
        subplot(2,3,l)
        image( xpyramid{l}(:,:,f)/2 )
        hold off
        title(sprintf( 'l(%d),f=%d',l,f ) )
    end
    drawnow
end

