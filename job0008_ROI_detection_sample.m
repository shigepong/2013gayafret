%% Set image-specific parameters
datapath = '/home/shige-o/data/2013GayaFRET/'
pyramid_level = 1;
switch 7
    case 5    
       fnbase = 'YFP_CFP5';
       qt_ratio = 0.10; qt_threshold = 85; corr_threshold = 0.8;
    case 7
       fnbase = 'YFP_CFP7';
       qt_ratio = 0.10; qt_threshold = 85; corr_threshold = 0.65;
end

adjacent = 4; % adjacent can be either 4 or 8

VL = 1; % visualization level. if it is higher, more plot windows appears.

tiffile = [datapath, fnbase, '.tif'];
ROIfile = [fnbase, '_ROI.mat'];

%% Load .tif image into variable  x
disp( sprintf( 'Loading %s', tiffile ) );

Nx = 270; Ny = 252; Nz = 1; Nf = 350;
info = imfinfo( tiffile );
xorg = zeros( Ny, Nx, Nf );
for f=1:Nf
    [x1, map] = imread( tiffile, f,  'Info',info );
    xorg( :, :, f ) = x1;
    if VL>1
        if f==1
            figure;
        end
        image( x1 )
        title(sprintf( 'f=%d',f ) )
        drawnow
    end
end
disp('Loaded')

%% im_pyramid
if pyramid_level==0
    x = xorg;
else
    disp('Constructing image pyramid' )
    xpyramid = im_pyramid( xorg, 5 );
    x = xpyramid{pyramid_level};
end


%% Segmentation of back-/fore-ground
[Nx,Ny,Nf] = size( x );

mo = quantile(x,qt_ratio,3);
[ix,iy] = meshgrid( 1:Ny, 1:Nx ); np = Nx*Ny; % Num. of all pixels
idx = mo(:)<qt_threshold; nfp = sum(idx); % Num. of fore-ground pixels
fpxid = zeros(1,np); fpxid( idx ) = 1:nfp; % fpxid at each PixelID. If fpxid( i ) == 0, the i-th pixel is in background.
pxid = find( idx ); % PixelID at each ForePixelID
if VL>0
  figure
  imagesc(mo)
  hold on
  plot( ix(idx), iy(idx), '.')
  title(sprintf('#Foreground pixel=%d', nfp))
end

%% Hint for segmentation parameter setting
if VL>0
    figure
    qt_ratio_candidate = [0.01, 0.1, 0.25, 0.5, 0.75];
    n_candidate = length(qt_ratio_candidate);
    for i=1:n_candidate
        subplot( 1, n_candidate, i )
        imagesc( quantile( x, qt_ratio_candidate(i), 3 ) )
        %contour( quantile( x, qt_ratio_candidate(i), 3 ) )
        title( sprintf('quantile ratio=%g', qt_ratio_candidate(i) ) )
    end
end


%% Prepare adjacent matrix
switch adjacent
    case 4
      nearest = [-1,1,-Nx,Nx];
    case 8
      nearest4 = [-Nx-1,-Nx,-Nx+1,-1,1,Nx-1,Nx,Nx+1];
end
A=zeros(nfp,nfp,'uint8'); % Adjacent matrix; if A(i,j)==1, pixels i and j are neighbourhood each other.
for fpx0=1:nfp
    px = pxid(fpx0);
    pxa = px + nearest;
    fpxa = fpxid( pxa );
    fpxa = fpxa( fpxa>0 );
    A(fpx0, fpxa) = 1;
end


%% Calculate Correlation between adjacent pixels
disp('Calculating correlation matrix')
R = zeros(nfp,nfp);
for fpx0=1:nfp
    fpx1 = find( A(fpx0,:)==1 );
    px0 = pxid( fpx0 );
    x0 = reshape( x( iy(px0), ix(px0), : ), Nf, 1 );
    for fpx=fpx1
        px1 = pxid( fpx );
        x1 = reshape( x( iy(px1), ix(px1), : ), Nf, 1 );
        R(fpx0,fpx) = corr( x0, x1 );
    end
end

%% Visualize Adjacency & Correlation
if VL>0
  disp('Visualizing adjacency & correlation')
  figure
  imagesc(mo)
  hold on

  rthresh = [0.7,0.8,0.9,0.95];
  stl = {'g-','b-','r-','b-','r-'};
  wdt = [1,1,3,3];
  for fpx0=1:nfp
    for s = 1:4
      fpx1 = find( R(fpx0,:)>rthresh(s) );
      for fpx=fpx1
         gpxid = pxid( [fpx0, fpx] );
         plot( ix( gpxid ), iy( gpxid ), stl{s}, 'LineWidth',wdt(s))
      end
    end
  end
end

%% Decompose the graph into connected components
disp('Decomposing graph')
[cidx,u] = dfs_connect_component( R > corr_threshold );
numROI = size( u, 2); % Number of ROIs
ROI.u = zeros( np, numROI, 'uint8' );
for k=1:numROI
    fpxk = find( cidx == k );
    gpxid = pxid( fpxk );
    ROI.u( gpxid, k ) = 1;
end
ROI.v = double(ROI.u)' * reshape( x, Nx*Ny, Nf );
ROI.u = reshape( ROI.u, Nx, Ny, numROI );

disp(sprintf('Saving %s', ROIfile))
save( ROIfile, 'ROI', 'mo' )

%% Visualize ROIs
if VL>0
  figure
  imagesc( mo )
  hold on
  for k=1:numROI
    u = ROI.u(:,:,k);
    id = find( u == 1 );
    c = rand(1,3);
    ixg = ix(id);
    iyg = iy(id);
    plot( ixg, iyg, '.', 'Color', c )
    text( mean(ixg), mean(iyg), sprintf('%d',k) )
  end
end

%% Visualize time courses
xo = reshape( x, Nx*Ny, Nf );
if VL>0
  figure
  for k=1:20
    u = ROI.u(:,:,k);
    idx = find( u==1 );
    npx = length(idx);
    if npx > 20
        idx = idx( randperm( npx, 20 ) );
    end
    vm = [];
    for i=1:length(idx)
      v0 = xo( idx(i), : );
      v0 = ( v0 - quantile( v0, qt_ratio ) )/std( v0 )/5;
      plot( v0+k, '-')
      hold on
      vm = [vm; v0];
    end
    plot( mean(vm)+k, 'r-', 'LineWidth',3)
    xlabel('Time point')
    ylabel('ROI index')
  end
end

