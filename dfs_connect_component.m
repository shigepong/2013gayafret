function [f,u] = dfs_connect_component( A )
% f = dfs_connect_component( A )
% Returns cluster of connected component
%  from an adjacent matrix  A
% Output  f(i)  denotes cluster index at the  i-th  node

n = length( A );
f = zeros( 1, n );
k = 1;
for i=1:n
    if f(i)==0
        f = dfs( A, f, i, k );
        k = k + 1;
    end
end

uf = unique(f);
nu = max(uf); count = zeros(1,nu);
for i=1:nu
    count(i) = sum( f==i );
end
[dum, id] = sort(-count);
f0 = f;
for i=1:nu
    f( f0==id(i) ) = i;
end

numcluster = sum( count > 1 );
u = sparse(n, numcluster);
for k=1:numcluster
    idx = find( f == k );
    u( idx, k ) = 1;
end


function fout = dfs( A, f, i, k )
%
n = length( f ); stack = zeros(n,1); sp = 1;
stack( sp ) = i;
while sp>0
  i = stack( sp );
  if f( i ) ~= 0
      sp = sp - 1;
  else
      f( i ) = k;
      idx = find( A(i, :) ~= 0 & f == 0 );
      for j0 = 1:length(idx)
         j = idx(j0);
         sp = sp+1;
         stack(sp) = j;
      end
  end
end
fout = f;
