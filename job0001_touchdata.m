datapath = '/home/shige-o/data/2013GayaFRET/'
fn1 = 'YFP_CFP5.tif';
fn2 = 'YFP_CFP7.tif';
fno = 'YFP_CFP5_CFP7.mat';
Nx = 270; Ny = 252; Nz = 1; Nf = 350;
Nc = 2;
tif1 = [datapath, fn1];
tif2 = [datapath, fn2];
info = imfinfo( tif1 );

x = zeros( Ny, Nx, Nc, Nf, 'uint8' );
x0 = zeros( Ny, Nx, 3, 'uint8' );
for f=1:Nf
    [x1, map] = imread( tif1, f,  'Info',info );
    [x2, map] = imread( tif2, f,  'Info',info );
    x0( :, :, 1 ) = x1;
    x0( :, :, 2 ) = x2;
    x( :, :, 1, f ) = x1;
    x( :, :, 2, f ) = x2;
    image( x0 )
    title(sprintf( 'f=%d',f ) )
    drawnow
end
%%
save( fno, 'x' )
%%
load( fno );
x0 = zeros( Ny, Nx, 3, 'uint8' );
for f=1:Nf
    x1 = x( :, :, 1, f );
    x2 = x( :, :, 2, f );
    x0( :, :, 1 ) = x1;
    x0( :, :, 2 ) = x2;
    image( x0 )
    hold off
    title(sprintf( 'f=%d',f ) )
    drawnow
end

