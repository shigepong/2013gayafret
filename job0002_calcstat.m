load( fno );
xorg = x;
xout = im_pyramid( x, 5 );
save('rec0002_pyramid.mat','xout')

%%

for l=3
    x = xout{l};
[Ny, Nx, Nc, Nf] = size( x );
m = mean( x, 4 );

x0 = zeros( Ny, Nx, 3, 'uint8' );
x0(:,:,1) = m(:,:,1);
x0(:,:,2) = m(:,:,2);
figure
subplot(2,2,1)
image( x0 )
title(sprintf('Scale#%d',l))
subplot(2,2,2)
m = reshape( m, Ny*Nx, Nc);
plot( m(:,1), m(:,2), '.', 'MarkerSize', 1)
xlabel('R(YFP CFP5)')
ylabel('G(YFP CFP7)')

v0 = reshape( x, Ny*Nx, Nc, Nf );
v0 = double(v0) - repmat( m, [1,1,Nf] );
size(v0)
v0 = max( v0, [], 3 );
v0 = reshape( sum( v0, 2), Ny, Nx );
x0(:,:,3)=uint8(v0);
subplot(2,2,3)
image(x0(:,:,3))
title('max(R)+max(G)')

subplot(2,2,4)
plot( m(:,1)+m(:,2), v0(:), '.', 'MarkerSize', 1 )
xlabel('average(R+G)')
ylabel('max(R)+max(G)-average(R+G)')

set(gcf,'Position',[100,400,800,600])
end

%%
l=3; v0_threshold = 20.0;
K = 10; % num ROIs

x=xout{l};
[Ny, Nx, Nc, Nf] = size( x );
m = mean( x, 4 );
m = reshape( m, Ny*Nx, Nc);
v0 = reshape( x, Ny*Nx, Nc, Nf );
v0 = double(v0) - repmat( m, [1,1,Nf] );
v0 = max( v0, [], 3 );
v0 = reshape( sum( v0, 2), Ny, Nx );
idx = (v0>v0_threshold);
ND = sum( idx(:));
disp( sprintf( 'Num. relevant pts = %d', ND ) )
x = reshape( x, Ny*Nx, Nc, Nf );
x = x(idx, :, : );

for c = 2
    % PCA
    y = reshape( x( :, c, : ), ND, Nf );
    m = mean( y, 2 );
    y = y - repmat( m, 1, Nf );
    [u,s,v] = svds( y, K );
    figure
    for k=1:K
        plot( 1:Nf, v(:,k) + k, '-' )
        hold on
    end
    % ICA
    [W,ss,vv] = kica( v' );
    v2 = v*W;
    recv{c} = v2;
    figure
    for k=1:K
        plot( 1:Nf, v2(:,k) + k, '-' )
        hold on
    end
end

%% Segmentation
x=xout{1};
[Ny, Nx, Nc, Nf] = size( x );
Img1 = zeros(Ny,Nx);
Img2 = zeros(Ny,Nx);
for ix = 1:Nx
  for iy = 1:Ny
    q = double( reshape( x(iy,ix,c,:), Nf, 1 ) );
    q = q-mean(q); q = q/sqrt( sum(q.^2));
    r = q' * v2; % Calculate correlation
    [a,b] = max( r ); % Calculate maximum correlation
    %and corresponding pattern index
    Img1( iy, ix ) = a;
    Img2( iy, ix ) = b;
  end
end
figure
subplot(2,2,1)
imagesc( Img1 )
subplot(2,2,2)
hist( Img1(:), 20 )
subplot(2,2,3)
imagesc( Img2 .* (Img1>0.4)  )
subplot(2,2,4)
hist( Img2(:), [1:K] )



    


%%
if false
close all
v = [rand(1000,2),[randn(500,1)-5;randn(500,1)+5]];
v = v * randn(3,3);
[W,ss,vv] = kica( v' )
v2 = v  * W;
figure
subplot(2,1,1)
plot( v(:,1), v(:,2), '.' )
hold on
plot( v2(:,1), v2(:,2), 'r.' )
subplot(2,1,2)
plot( v(:,1), v(:,3), '.' )
hold on
plot( v2(:,1), v2(:,3), 'r.' )
end
