datapath = '/home/shige-o/data/2013GayaFRET/'
switch 5
    case 5    
       fnbase = 'YFP_CFP5';
       qt_ratio = 0.10; qt_threshold = 85;
    case 7
       fnbase = 'YFP_CFP7';
       qt_ratio = 0.10; qt_threshold = 80;
end
matfile = [fnbase '.mat'];
load(matfile)

%%
l=1
xo = xpyramid{l};

[Nx,Ny,Nf] = size( xo );
%% Segmentation
mo = quantile(xo,qt_ratio,3);
[ix,iy] = meshgrid( 1:Ny, 1:Nx ); np = Nx*Ny; % #pixels
idx = mo(:)<qt_threshold; nfp = sum(idx); % #fore.pixels
fpxid = zeros(1,np); fpxid( idx ) = 1:nfp; % ForePixelID
pxid = find( idx ); % PixelID at each ForePixelID

figure
image(mo*0.4)
hold on
plot( ix(idx), iy(idx), '.')
title(sprintf('#Foreground pixel=%d', nfp))


%% Calculate Adjacent matrix
nearest4 = [-1,1,-Nx,Nx];
%nearest4 = [-Nx-1,-Nx,-Nx+1,-1,1,Nx-1,Nx,Nx+1];
A=zeros(nfp,nfp,'uint8'); % Adjacent matrix
for fpx0=1:nfp
    px = pxid(fpx0);
    px4 = px + nearest4;
    fpx4 = fpxid( px4 );
    fpx4 = fpx4( fpx4>0 );
    A(fpx0, fpx4) = 1;
%    plot( ix(px4), iy(px4), 'r.')
end


%% Calculate Correlation between Adjacent pixels
R = zeros(nfp,nfp);
for fpx0=1:nfp
    fpx1 = find( A(fpx0,:)==1 );
    px0 = pxid( fpx0 );
    xo0 = reshape( xo( iy(px0), ix(px0), : ), Nf, 1 );
    for fpx=fpx1
        px1 = pxid( fpx );
        xo1 = reshape( xo( iy(px1), ix(px1), : ), Nf, 1 );
        R(fpx0,fpx) = corr( xo0, xo1 );
    end
end

%% Visualize Adjacency & Correlation
figure
image(mo*0.4)
hold on

rthresh = [0.7,0.8,0.9,0.95];
stl = {'b-','r-','b-','r-'}
wdt = [1,1,3,3];
for fpx0=1:nfp
    for s = 1:4
    fpx1 = find( R(fpx0,:)>rthresh(s) );
    for fpx=fpx1
       gpxid = pxid( [fpx0, fpx] );
       plot( ix( gpxid ), iy( gpxid ), stl{s}, 'LineWidth',wdt(s))
    end
    end
end

%% Decompose into connected components
[cidx,u] = dfs_connect_component( R > 0.8 );
numROI = size( u, 2); % Number of ROIs
ROI.u = sparse( np, numROI );
for k=1:numROI
    fpxk = find( cidx == k );
    gpxid = pxid( fpxk );
    ROI.u( gpxid, k ) = 1;
end
ROI.v = ROI.u' * reshape( xo, Nx*Ny, Nf );

figure
image( mo*0.4 )
hold on
for k=1:numROI
    %pxk = find( u(:,k) );
    %gpxid = pxid( pxk );
    gpxid = find( ROI.u(:,k) == 1 );
    c = rand(1,3);
    plot( ix( gpxid), iy(gpxid), '.', 'Color', c )
end
%%
figure
for k=1:10
    v0 = ROI.v(k,:);
    v0 = ( v0 - quantile( v0, qt_ratio ) )/std( v0 )/5;
    plot( v0+k, '-')
    hold on
end


