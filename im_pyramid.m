function xout = im_pyramid( x, lmax )

[Nx, Ny, Nz, Nf] = size( x );
if Nf==1
    [Nx, Ny, Nf] = size( x );
for l=1:lmax
    l
    Nx1 = floor( Nx / (2^l) );
    Ny1 = floor( Ny / (2^l) );
    for f = 1:Nf
       tmp = imresize( x(:,:,f), 1/(2^l) );
       if f==1
          xout{l} = zeros( size(tmp,1), size(tmp,2), Nf );
       end
       xout{l}(:,:,f) = tmp;          
    end        
end

else
    
for l=1:lmax
    l
    Nx1 = floor( Nx / (2^l) );
    Ny1 = floor( Ny / (2^l) );
    for z = 1:Nz
        for f = 1:Nf
            tmp = imresize( x(:,:,z,f), 1/(2^l) );
            if z==1 & f==1
                xout{l} = zeros( size(tmp,1), size(tmp,2), Nz, Nf );
            end
            xout{l}(:,:,z,f) = tmp;          
        end
    end        
end

end


