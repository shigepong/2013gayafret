load('rec0002_pyramid.mat') %for x_out

%
%%

l=2

x = xout{l};
[Ny, Nx, Nc, Nf] = size( x );
m = mean( x, 4 );

x0 = zeros( Ny, Nx, 3, 'uint8' );
x0(:,:,1) = m(:,:,1);
x0(:,:,2) = m(:,:,2);
figure
subplot(2,2,1)
image( x0 )
title(sprintf('Scale#%d',l))
subplot(2,2,2)
m = reshape( m, Ny*Nx, Nc);
plot( m(:,1), m(:,2), '.', 'MarkerSize', 1)
xlabel('R(YFP CFP5)')
ylabel('G(YFP CFP7)')

intensity = m(:,1)+m(:,2);

v0 = reshape( x, Ny*Nx, Nc, Nf );
v0 = double(v0) - repmat( m, [1,1,Nf] );
size(v0)
v0 = max( v0, [], 3 );
maxchange = reshape( sum( v0, 2), Ny, Nx );

x0(:,:,3)=uint8(maxchange);
subplot(2,2,3)
image(x0(:,:,3))
title('max(R)+max(G)')

subplot(2,2,4)
plot( intensity(:), maxchange(:), '.', 'MarkerSize', 1 )
xlabel('average(R+G)')
ylabel('max(R)+max(G)-average(R+G)')

set(gcf,'Position',[100,400,800,600])

%%
l=2; max_threshold = 14.0;intensity_threshold=200;
K = 15; % num features per a channel
KC = 15; % num clusters

x=xout{l};
[Ny, Nx, Nc, Nf] = size( x );
m = mean( x, 4 );
intensity = sum(m,3);
m = reshape( m, Ny*Nx, Nc);
v0 = reshape( x, Ny*Nx, Nc, Nf );
v0 = double(v0) - repmat( m, [1,1,Nf] );
v0 = max( v0, [], 3 );
maxchange = reshape( sum( v0, 2), Ny, Nx );
idx = (maxchange>max_threshold & intensity<intensity_threshold);

figure
subplot(2,1,1)
imagesc( reshape(idx, Ny, Nx) )

ND = sum( idx(:));
disp( sprintf( 'Num. relevant pts = %d', ND ) )
x = reshape( x, Ny*Nx, Nc, Nf );
x = x(idx, :, : );
figure
for c = 1:2
    % PCA
    y = reshape( x( :, c, : ), ND, Nf );
    m = mean( y, 2 );
    y = y - repmat( m, 1, Nf );
    [u,s,v] = svds( y, K );
    recv{c} = v;
    
    % ICA
    W = kica( v' );
    v2 = v*W;
    recvICA{c} = v2;
    
    subplot(2,2,c)
    for k=1:K
        plot( 1:Nf, v(:,k)*3 + k, '-' )
        hold on
    end
    ylim([0,K+1])
    title(sprintf('Ch%d PCA',c))
    subplot(2,2,c+2)
    for k=1:K
        plot( 1:Nf, v2(:,k)*3 + k, '-' )
        hold on
    end
    ylim([0,K+1])
    title(sprintf('Ch%d ICA',c))
end

%% Calc feature
x=xout{2};
[Ny, Nx, Nc, Nf] = size( x );
ImgF = zeros(Ny,Nx,2*K);
for ix = 1:Nx
  for iy = 1:Ny
      for c=1:2
    q = double( reshape( x(iy,ix,c,:), Nf, 1 ) );
    q = q-mean(q); q = q/sqrt( sum(q.^2));
    r = q' * recv{c}; % Calculate correlation
    ImgF(iy, ix, (1:K)+K*(c-1) ) = r;
      end
%    ImgF(iy, ix, (1:2)+(K*2)) = mean(x(iy,ix,:,:),4)/30;
  end
end

%%
Y = reshape( ImgF, Nx*Ny, 2*K );
[IDX, C, SUMD, D] = kmeans( Y(idx,:), KC );
dum = zeros(Ny, Nx);
dum(idx) = IDX;

subplot(2,1,2)
imagesc( reshape(dum,Ny,Nx))
%%
figure
x2 = reshape( x, Nx*Ny, Nc, Nf );
for k=1:KC
    idxk = find( dum==k );
    for c=1:2
       a = reshape( x2( idxk, c, : ), length(idxk), Nf );
       a = a';
       a = (a-repmat(mean(a),Nf,1)) ./repmat(std(a),Nf,1);
       subplot(1,2,c)
       plot( a+k*10, 'b-')
       hold on
    end
end



