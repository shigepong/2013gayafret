datapath = '/home/shige-o/data/2013GayaFRET/'
fnbase = 'YFP_CFP5';
%fnbase = 'YFP_CFP7';
matfile = [fnbase '.mat'];
load(matfile)

%%
for l=1:1
    
xo = xpyramid{l};
[Nx,Ny,Nf] = size( xo );

% --- segmentation ---
%mo = median( xo, 3 );
mo = quantile(xo,0.10,3);
if false
  figure
  subplot(2,3,1)
  imagesc( median(xo,3)*0.4 );
  title( 'Median' )
  subplot(2,3,2)
  imagesc( max(xo,[],3)*0.4 );
  title( 'Max' )
  subplot(2,3,3)
  imagesc( min(xo,[],3)*0.4 );
  title( 'Min' )
  subplot(2,3,4)
  imagesc( quantile(xo,0.25,3)*0.4 );
  title( '25 percentile' )
  subplot(2,3,5)
  imagesc( quantile(xo,0.10,3)*0.4 );
  title( '10 percentile' )
end

n0 = 3;

[ix,iy] = meshgrid( 1:(Nx-n0), 1:Ny );
n = length(ix(:));
r = zeros(n,n0);
for i=1:n
    m(i) = mo( ix(i), iy(i) );
    x0 = reshape( xo(ix(i)+(0:n0), iy(i), 1:Nf), n0+1, Nf );
    x0 = x0 - repmat( mean(x0,2), 1,Nf);
    d0 = sqrt(sum( x0.^2, 2 ));
    x0 = x0 ./ repmat( d0, 1,Nf );
    r(i,:) = x0( 1, : ) * x0( 2:n0+1, : )';
end

figure;
plot( m, r, '.' )
%xlabel( 'Average')
xlabel( '10 percentile')
ylabel( 'Corr.coeff')
title(sprintf('L(%d)',l))
ylim([-1,1])
xlim([40,125])
%%
figure;
l = 1; threshold = 95;
for cs = 1:2
    switch cs
        case 1
          idx = (m>threshold);
          casename = 'Background';
        case 2
          idx = (m<threshold);
          casename = 'Foreground';
    end
    bin = [-1:0.05:1];
    ht = [];
    for d = 1:n0
       ht = [ht; hist( r(idx,d), bin )];
    end
    subplot( 1, 2, cs )
    plot( bin, ht )
    xlabel('Corr.coeff')
    title( casename )
end
%%
figure
image(mo*0.4)
idx = ( m<threshold & r(:,1)'>0.8 );
hold on
plot( iy(idx), ix(idx), '.')
[ix0,iy0] = meshgrid( 1:Ny, 1:Nx );
idx = (mo>threshold);
plot( ix0(idx), iy0(idx), 'k.', 'MarkerSize',1)
end

